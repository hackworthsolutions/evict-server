FROM node:7.2.1

# Install dependencies
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb http://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get update -qq && apt-get install -y yarn \
  && apt-get clean \

  # Add 'evict' user which will run the application
  && adduser evict --home /home/evict --shell /bin/bash --disabled-password --gecos ""

ENV HOME=/home/evict

COPY package.json yarn.lock $HOME/app/
RUN chown -R evict:evict $HOME/*

USER evict
WORKDIR $HOME/app
RUN yarn install

USER root
COPY . $HOME/app
RUN chown -R evict:evict $HOME/*

USER evict
CMD ["yarn", "start"]
