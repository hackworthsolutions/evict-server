let ready = false;
let rooms = {};
let users = {};

module.exports = {
  API: {
    init: (serviceId, serviceKey, nuve_host) => {
      ready = true;
      return ready;
    },
    createRoom: (name, resolve, reject, opts = {}) => {
      if(!ready) return reject();
      let _id = Date.now() + '' + String(Math.random()).substr(2);
      while(rooms[_id]) {
        _id = Date.now() + '' + String(Math.random()).substr(2);
      };
      rooms[_id] = {
        _id,
        name,
        p2p: opts.p2p || false,
        data: opts && Object.assign({}, opts, { p2p: undefined }) || {}
      };
      users[_id] = [];
      return resolve(rooms[_id]);
    },
    getRooms: (resolve, reject) => {
      if(!ready) return reject();
      return resolve( JSON.stringify(Object.values(rooms)) );
    },
    getRoom: (id, resolve, reject) => {
      if(!ready) return reject();
      if(!rooms[id]) return reject({ code: 404, text: 'Not found' });
      return resolve( JSON.stringify(Object.values(rooms[id])) );
    },
    deleteRoom: (id, resolve, reject) => {
      if(!ready) return reject();
      if(!rooms[id]) return reject({ code: 404, text: 'Not found' });
      const res = rooms[id];
      delete rooms[id];
      delete users[id];
      return resolve(res);
    },
    createToken: (roomId, name, role, resolve, reject) => {
      if(!ready) return reject();
      if(!rooms[roomId]) return reject({ code: 404, text: 'Not found' });
      const token = `${ roomId }_${ String(Math.random()).substr(2) }`;
      users[roomId].push({ name, role, token });
      return resolve(token);
    },
    getUsers: (roomId, resolve, reject) => {
      if(!ready) return reject();
      if(!users[roomId]) return reject({ code: 404, text: 'Not found' });
      return resolve( JSON.stringify( users[roomId] ) );
    },
  }
};

