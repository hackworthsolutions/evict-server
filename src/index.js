const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const routes = require('./routes');
const N = require('../vendor/licode/nuve');
const config = require('../config');

const app = express();
const port = process.env.NODE_PORT || 4000;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/', routes);

N.API.init(
  config.LICODE_SERVICE_ID,
  config.LICODE_SERVICE_KEY,
  config.LICODE_NUVE_HOST
);

app.listen(port, function () {
  console.log(`Example app listening on port ${port}!`);
});
