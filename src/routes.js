const express = require('express');
const room = require('./controllers/rooms');

const router = express.Router();

router.get('/', (req, res, next) => {
  res.send('Hello World!');
});

router.get('/rooms', room.list);
router.post('/rooms', room.create);
router.delete('/rooms/:id', room.destroy);
router.get('/rooms/:id', room.read);
router.put('/rooms/:id', room.update);
router.post('/rooms/:id/token', room.createToken);
router.delete('/rooms/:id/token', room.destroyToken);
  
module.exports = router;
