const N = require('../../vendor/licode/nuve');

const destroy = (id) => {
  return new Promise ((resolve, reject) => N.API.deleteRoom(id, resolve, reject));
};

const list = () => {
  return new Promise ((resolve, reject) => N.API.getRooms(resolve, reject))
  .then(payload => JSON.parse(payload))
  //.then(rooms => rooms.filter(r => r.data && r.data.public));
};

const read = (id) => {
  return new Promise ((resolve, reject) => N.API.getRoom(resolve, reject))
  .then(payload => JSON.parse(payload))
};

const create = (data) => {
  return new Promise ((resolve, reject) => N.API.createRoom(data.name, resolve, reject, Object.assign({}, data, { name: undefined })));
};

const update = (id, room) => {
  return new Promise ((resolve, reject) => reject({ code: 405, text: 'Method not allowed' }));
};

const createToken = (id, data) => {
  return new Promise ((resolve, reject) => N.API.createToken(id, data.name, data.role, resolve, reject));
};

const destroyToken = (id) => {
  return new Promise ((resolve, reject) => reject({ code: 405, text: 'Method not allowed' }));
};

module.exports = {
  destroy,
  list,
  read,
  create,
  update,
  createToken,
  destroyToken,
};
