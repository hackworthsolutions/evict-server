const room = require('../models/room');

const successHandler = (res, code) => (payload) => {
  console.log('SUCCESS', payload);
  res.status(code || 200).json(payload);
};

const errorHandler = (res) => (error) => {
  console.log('ERROR', error);
  res.status(error.code || 500).send(error || 'Server-side error');
};

const destroy = (req, res) => {
  room.destroy(req.params.id)
    .then(successHandler(res))
    .catch(errorHandler(res));
};

const list = (req, res) => {
  room.list()
    .then(successHandler(res))
    .catch(errorHandler(res));
};

const read = (req, res) => {
  room.read(req.params.id)
    .then(successHandler(res))
    .catch(errorHandler(res));
};

const update = (req, res) => {
  room.update(req.params.id, req.body)
    .then(successHandler(res))
    .catch(errorHandler(res));
};

const create = (req, res) => {
  room.create(req.body)
    .then(successHandler(res, 201))
    .catch(errorHandler(res));
};

const createToken = (req, res) => {
  room.createToken(req.params.id, req.body)
    .then(successHandler(res, 201))
    .catch(errorHandler(res));
};

const destroyToken = (req, res) => {
  room.destroyToken(req.params.id)
    .then(successHandler(res))
    .catch(errorHandler(res));
};


module.exports = {
  destroy,
  list,
  read,
  update,
  create,
  createToken,
  destroyToken,
};
